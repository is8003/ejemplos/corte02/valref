#include <stdio.h>

// Prototipo de funciones
int cuadradoVal(int n);
void cuadradoRef(int *nPtr);

int main(void)
{
   int num = 5; // Inicializo num

   // Imprimo valor inicial de número
   printf("Valor inicial de número es: %d\n", num);
   
   // Pasa num a cuadradoVal() por valor
   printf("El cuadrado (por valor) es %d\n", cuadradoVal(num));

   // Imprimo valor de número
   printf("Valor de número se mantiene: %d\n", num);

   // Invoco cuadrado por referencia
   cuadradoRef(&num);

   // Pasa num a cuadradoVal() por valor
   printf("El cuadrado (por referencia) es %d\n", num);

   // Imprimo valor de número
   puts("¡Valor de variable num ha sido modificado!");
} 

// Calculo y retorno el cuadrado del número recibido como parámetro
int cuadradoVal(int n)                                            
{                                                                   
   return n * n ; // cuadrado de la variable local n y retorno
}

// Calculo cuadrado de *nPtr, está modificando variable en main.
void cuadradoRef(int *nPtr)                              
{                                                              
   *nPtr = *nPtr * *nPtr; // cuadrado *nPtr            
}
