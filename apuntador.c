#include <stdio.h>

int main(void)
{
   int num = 5;
   int *numPtr = &num; // Asigna dirección de num a numPtr

   printf("La dirección de num es %p"
          "\nEl valor de numPtr es %p", &num, numPtr);

   printf("\n\nEl valor de num es %d"   
          "\nEl valor de *numPtr es %d", num, *numPtr);

   printf("\n\nOperadores * y & son complemento entre sí\n&*numPtr = %p"   
          "\n*&numPtr = %p\n", &*numPtr, *&numPtr);
} 
